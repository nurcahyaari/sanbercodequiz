import React, {useContext} from 'react';
import {FlatList, View, Text} from 'react-native';
import {RootContext} from './Soal2';

const Flatlist = () => {
  const state = useContext(RootContext);
  return (
    <FlatList
      data={state.name}
      renderItem={({item}) => (
        <View>
          <Text>{item.name}</Text>
        </View>
      )}
      keyExtractor={(item) => item.name}
    />
  );
};

export default Flatlist;
